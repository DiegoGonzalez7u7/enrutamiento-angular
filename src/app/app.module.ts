import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PrimeroComponent } from './components/primero/primero.component';
import { SegundoComponent } from './components/segundo/segundo.component';
import { TerceroComponent } from './components/tercero/tercero.component';
import { CuartoComponent } from './components/cuarto/cuarto.component';
import { QuintoComponent } from './components/quinto/quinto.component';
import { HomeComponent } from './components/home/home.component';

//dentro del module se importan la hoja de enrutamiento y el modulo de enrutamiento
import {RouterModule} from '@angular/router'
import { ROUTES } from './app.routes';
//dentro del module se importan la hoja de enrutamiento y el modulo de enrutamiento

@NgModule({
  declarations: [
    AppComponent,
    PrimeroComponent,
    SegundoComponent,
    TerceroComponent,
    CuartoComponent,
    QuintoComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES,{useHash:true}), //en los imports agregamos el routermodule para que reconozca la 
    //navegacion por rutas 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
