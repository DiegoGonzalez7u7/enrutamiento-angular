import { Routes } from "@angular/router";
import { CuartoComponent } from "./components/cuarto/cuarto.component";
import { HomeComponent } from "./components/home/home.component";
import { PrimeroComponent } from "./components/primero/primero.component";
import { QuintoComponent } from "./components/quinto/quinto.component";
import { SegundoComponent } from "./components/segundo/segundo.component";
import { TerceroComponent } from "./components/tercero/tercero.component";

export const ROUTES: Routes=[
    //paths o direcciones a los cuales haremos referencia a nuestros componentes
    {path:'home', component:HomeComponent},
    {path:'componente/uno', component: PrimeroComponent},
    {path:'componente/dos', component: SegundoComponent},
    {path:'componente/tres', component: TerceroComponent},
    {path:'componente/cuatro', component: CuartoComponent},
    {path:'componente/cinco', component: QuintoComponent},
    //paths o direcciones a los cuales haremos referencia a nuestros componentes
    
    //estas dos ultimas lineas siempre van al final
    {path:'', pathMatch:'full', redirectTo:'home'}, //sirven para señalar a nuestro primer componente
    {path:'**', pathMatch:'full', redirectTo:'home'},
    //estas dos ultimas lineas siempre van al final
];
